package com.casestudy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.UserService;
import com.casestudy.validators.LoginValidator;
import com.casestudy.validators.PetValidator;
import com.casestudy.validators.UserValidator;




@Controller
public class MainController {
	@Autowired
	UserService userService;
	@Autowired
	UserValidator userValidator;
	@Autowired
	LoginValidator loginValidator;
	@Autowired
	PetValidator petValidator;

	@GetMapping(value = "/")
	public ModelAndView index(@ModelAttribute("user") User user) {

		ModelAndView modelAndView = new ModelAndView("registrationPage");
		return modelAndView;
	}

	@PostMapping(value = "saveUser")
	public ModelAndView saveuser(@ModelAttribute("user") User user, BindingResult results) {
		userValidator.validate(user, results);
		if (results.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("registrationPage");
			return modelAndView;
		}
		ModelAndView modelAndView = new ModelAndView("loginPage");
		user = userService.saveUser(user);
		modelAndView.addObject("successmessage", "you have Successfully Registered");
		return modelAndView;
	}

	@PostMapping(value = "authenticateUser")
	public ModelAndView authenticateUser(HttpServletRequest request, @ModelAttribute("user") User user,
			BindingResult results) {

		loginValidator.validate(user, results);

		if (results.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("loginPage");
			return modelAndView;
		}

		else {

			ModelAndView modelAndView = new ModelAndView("newPage");
			modelAndView.addObject("message", "You have successfully Logged In");
			return modelAndView;

		}
	}
	
	@RequestMapping(value="home")
	public String home(@ModelAttribute("pet") Pet pet)
	{
		return "addPetPage";
	}
	@PostMapping(value="savePet")
	public ModelAndView savePet(@ModelAttribute("pet") Pet pet,BindingResult results) {
		petValidator.validate(pet,results);
		if(results.hasErrors())
		{
			ModelAndView modelAndView=new ModelAndView("addPetPage");
			return modelAndView;
		}
		userService.savePet(pet);
		System.out.println(pet.getPetName());
		ModelAndView modelAndView=new ModelAndView("homePage");
		modelAndView.addObject("message","Pet Added Successfully");
		return modelAndView;
	}
	@RequestMapping(value="homePage")
	public ModelAndView petList(HttpServletRequest request)
	{
		List<Pet> pets;
		pets = userService.getAllPets();
		 for (Pet pet : pets) {
	    	 System.out.println(pet.getPetName());
	    	 
	    	 
	    	 
				    	 
			
		}
		ModelAndView modelAndView=new ModelAndView();
		
		modelAndView.addObject("pets", pets);
		return modelAndView;
	}
}
