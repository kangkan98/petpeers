package com.casestudy.service;




import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casestudy.dao.UserDAO;
import com.casestudy.model.Pet;
import com.casestudy.model.User;


@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserDAO userDao;

	@Transactional
	public User saveUser(User user) {
		userDao.saveUser(user);
		return user;
	}

	@Transactional
	public boolean checkUserName(String userName) {

		boolean flag = userDao.checkUserName(userName);
		return flag;
	}
	@Transactional
	public User authenticateUser(String userName, String password) {
		User user;
		user=userDao.authenticateUser(userName, password);
		return user;
	}
	@Transactional
	public Pet savePet(Pet pet) {
		
		pet=userDao.savePet(pet);
		return pet ;
	}
	@Transactional
	public List<Pet> getAllPets() {
		List<Pet> pets;
		pets = userDao.getAllPets();
		return pets;
	}

}

