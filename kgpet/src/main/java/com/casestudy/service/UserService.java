package com.casestudy.service;



import java.util.List;

import com.casestudy.model.Pet;
import com.casestudy.model.User;


public interface UserService {
	public abstract User saveUser(User user);

	public abstract User authenticateUser(String userName, String password);

	public abstract boolean checkUserName(String userName);

	public abstract Pet savePet(Pet pet);
	
	public abstract List<Pet> getAllPets();
}
