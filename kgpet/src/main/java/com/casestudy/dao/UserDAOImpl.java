package com.casestudy.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.casestudy.model.Pet;
import com.casestudy.model.User;

@Repository
public class UserDAOImpl implements UserDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public User saveUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(user);
		System.out.println("data saved succesfully");
		return user;
	}

	public boolean checkUserName(String userName) {

		Session session = this.sessionFactory.getCurrentSession();
		User user = (User) session.createQuery("FROM User U WHERE U.userName= ?").setParameter(0, userName)
				.uniqueResult();
		if (user != null) {
			return true;
		}
		return false;
	}

	public User authenticateUser(String userName, String password) {

		User user = null;
		Session session = this.sessionFactory.getCurrentSession();
		user = (User) session.createQuery("FROM User U WHERE U.userName= ? AND U.userPassword= ?")
				.setParameter(0, userName).setParameter(1, password).uniqueResult();
		if (user == null) {
			user = (User) session.createQuery("FROM User U WHERE U.userName= ? ").setParameter(0, userName)
					.uniqueResult();
		}
		return user;
	}

	public Pet savePet(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(pet);
		return pet;
	}

	public List<Pet> getAllPets() {
		
		
		
		Session session = this.sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<Pet> pets =(List<Pet>) session.createQuery(" FROM Pet").list();
		
		
	    /** for (Pet pet : pets) {
	    	 System.out.println(pet.getPetName());
	    	 
	    	 
	    	 
				    	 
			
		} **/
	
		
		return pets;
	}

}
