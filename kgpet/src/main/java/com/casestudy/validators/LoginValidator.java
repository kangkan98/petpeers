package com.casestudy.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.User;
import com.casestudy.service.UserService;


@Component
public class LoginValidator implements Validator {
	@Autowired
	UserService userService;

	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "userName", "user.userName.empty");
		ValidationUtils.rejectIfEmpty(errors, "userPassword", "user.userPassword.empty");
		User user = (User) target;
		String userName = user.getUserName();
		String password = user.getUserPassword();

		if (userName != "" && password!="") {
			User user1 = userService.authenticateUser(userName, password);
			if(user1==null)
			{
				errors.rejectValue("userName", "user.userName.nouser");
			}
			else if (!((user1.getUserName().equals(userName))&&(user1.getUserPassword().equals(password)))) {
				errors.rejectValue("userPassword", "user.userPassword.error");
			}
		}
	}

}

