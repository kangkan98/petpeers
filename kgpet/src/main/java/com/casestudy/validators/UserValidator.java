package com.casestudy.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.User;
import com.casestudy.service.UserService;

@Component
public class UserValidator implements Validator {

	@Autowired
	UserService userService;

	public boolean supports(Class<?> clazz) {

		return User.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {

		ValidationUtils.rejectIfEmpty(errors, "userName", "user.userName.empty");
		ValidationUtils.rejectIfEmpty(errors, "userPassword", "user.userPassword.empty");
		ValidationUtils.rejectIfEmpty(errors, "confirmPassword", "user.confirmPassword.empty");

		User user = (User) target;
		String userName = user.getUserName();
		String password = user.getUserPassword();
		String confirmPassword = user.getConfirmPassword();

		if (userName != "") {
			boolean flag = userService.checkUserName(userName);
			if (flag) {
				errors.rejectValue("userName", "user.userName.exist");
			}
		}
		if (!(password.equals(confirmPassword))) {
			errors.rejectValue("confirmPassword", "user.confirmPassword.notmatch");
		}

	}

}
