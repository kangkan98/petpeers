<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="/PetPeer/styles.css">
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="script.js"></script>
<title>My Pets</title>
</head>
<body>

<div id='cssmenu'>
<ul>
   <li><a href='/jsp/homePage'><span>Home</span></a></li>
   <li class='active'><a href='/jsp/myPetsPage'><span>My Pets</span></a></li>
   <li><a href='/PetPeer/views/jsp/addPetPage.jsp'><span>Add Pet</span></a></li>
   <li><a href='/PetPeer/views/jsp/logout.jsp'><span>Logout</span></a></li>
</ul>
</div>
<center>
<h2>Pet List</h2>
        
        <table border="1" width="500" height="200">
			<tr>
				<td width="119" align="center"><font size="5sp"><b>#</b></font></td>
				<td width="168" align="center" ><font size="5sp"><b>Pet Name</b></font></td>
				<td width="168" align="center"><font size="5sp"><b>Place</b></font></td>
				<td width="119" align="center"><font size="5sp"><b>Age</b></font></td>
			</tr>
	
		<tr>
			<td width="119" align="center"><font size="4sp">DATA1</font></td>
			<td width="168" align="center"><font size="4sp">DATA2</font></td>
			<td width="168" align="center"><font size="4sp">DATA3</font></td>
			<td width="119" align="center"><font size="4sp">DATA4</font></td>
		</tr>
		
		</table>
</center>
</body>
</html>