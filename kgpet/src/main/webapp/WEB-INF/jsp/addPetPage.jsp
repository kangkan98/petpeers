

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Page</title>
<style>
#div1 {
	left: 32%;
	top: 140px;
	height: 419px;
	width: 511px;
	border-style: double;
	border-color: black;
	border-radius: 6pt;
	background-color: #f5f7f7;
	position: relative;
}

#header {
	overflow: hidden;
	background-color: white;
}

.navmenu {
	border-radius: 1pt;
	color: #000;
	text-align: center;
	width: 100px;
	height: 60px;
	padding: 20px;
	text-decoration: none;
	font-family: fantasy;
	font-size: 17px;
	font-weight: bold;
}

.navmenu:hover {
	background-color: aqua;
	color: maroon;
}

.navmenu:active {
	background-color: gray;
	color: maroon;
}

.image {
	float: left;
}
</style>
</head>
<body style="background-color: #e8eded; margin: 0; height: 845px;">

	<div class="image">
		<img src="./pet-shop-logo-vector-15498684.jpg" alt="logo" height="61"
			width="92" />
	</div>

	<div id="header" style="height: 61px">
		<a style="float: left;" class="navmenu" href="#">Home</a> <a
			style="float: left;" class="navmenu" href="#">Page1</a> <a
			style="float: right;" class="navmenu" href="#"> <img
			src="images.jpg" alt="login-logo" height="23" width="20" /></a>
	</div>

	<div id="div1">

		<form:form action="savePet" method="post" modelAttribute="pet" style="height:419px; width: 511px;">

			<h2
				style="font-family: sans-serif; text-align: center; font-style: oblique; top: 20px; position: relative">Register</h2>

			<table
				style=" width: 348px; left: 16%; position: absolute; top: 104px; height: 215px;">

				<tr>
					<td style="font-family: Georgia, serif; font-size: 13pt;">PetName:</td>
					<td><form:input path="petName" style="border-color: #000;"/></td>
					
				</tr>
				<tr><td></td><td><form:errors path="petName" style="color:red;"/></td></tr>
				<tr>
					<td style="font-family: Georgia, serif; font-size: 13pt;">Age:</td>
					<td><form:input path="petAge" style="border-color: #000;"/></td>
					
				</tr>
				<tr><td></td><td><form:errors path="petAge" style="color:red;"/></td></tr>
				<tr>
					<td style="font-family: Georgia, serif; font-size: 13pt;">Place:</td>
					<td><form:input path="petPlace" style="border-color: #000;"/></td>
				</tr>
				<tr><td></td><td><form:errors path="petPlace" style="color:red;"/></td></tr>
				<tr>
					<td></td>
					<td><input type="submit" name="Submit" value="Register"
						style="width: 90px; height: 30px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; font-weight: bold;"></td>
					<td></td>
				</tr>
			</table>
		</form:form>

	</div>

</body>
</html>