%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
<style>
#div1{
	width:500px;
	height:500px;
	border-style:double;
	border-color:black;
	border-radius:6pt;
	background-color: #f5f7f7;
	top:150px;
	left:35%;
	position:relative;
	}
	#header{
	overflow:hidden;
	background-color:white;
	}

.navmenu{
		border-radius:1pt;
		color: #000;
		text-align: center;
		width:100px;
		height:60px;
		padding: 20px;
		text-decoration: none;
		font-family:fantasy;
		font-size: 17px;
		font-weight: bold;
	}
	.navmenu:hover {
	background-color: aqua;
	color:maroon;
}
	.navmenu:active{
	background-color: gray;
	color:maroon;

	
}
.image{
float:left;
}

</style>
</head>
<body  style="background-color:#e8eded; margin:0;">

	<div class="image">
	<img src="pet-shop-logo-vector-15498684.jpg" alt="logo" height="61" width="92" />
	</div>
	
	<div id="header" style="height: 61px">
		<a style="float: left;" class="navmenu" href="button.html">Home</a>
		<a style="float: left;" class="navmenu" href="#">Page1</a>
		<a style="float: right;"class="navmenu"  href="#" >
		<img  src="images.jpg" alt="login-logo" height="23" width="20" /></a>
	</div> 
	<div id="div1" >
		<form:form action="authenticateUser" method="post" modelAttribute="user" style="height:500px;">
			<h2 style="font-family:sans-serif;text-align:center; font-style:oblique; top:20px;position:relative">LOGIN</h2>
			<div style=" width:250px; left:75px;top:30px; position:relative;">
				<span style="color:green;">${successmessage}</span>
				<table style="line-height:50px;">
				
					<tr>
						<td style="font-family: Georgia ,serif;font-size: 13pt;">UserName:</td>
						<td><form:input path="userName" style="border-color: #000;"/></td>
					</tr>
					<tr>
					<td></td>
					<td><form:errors path="userName" style="color: red;"/></td>
					</tr>
					<tr>
						<td style="font-family: Georgia  , serif ;font-size: 13pt;" >Password:</td>
						<td><form:password path="userPassword" style="border-color: #000;"/></td>
					</tr>
					<tr>
					<td></td>
					<td><form:errors path="userPassword" style="color:red;"/></td>
					</tr>
					<tr>
					<td></td>
					<td ><input type="submit" name="Submit" value="LogIn" style="width:80px;height:30px;font-size: 15px; font-family:inherit; ;font-weight:bold;"></td>
					</tr>
				</table>
				</div>
		</form:form>

	</div>

</body>
</html>